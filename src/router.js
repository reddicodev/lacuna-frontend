import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
import store from './store'
import Login from './views/auth/Login.vue'
import NotFound from './views/NotFound.vue'
import Home from './views/Home.vue'
// import Workspaces from './views/workspace/view-all.vue'
// import WorkspaceSingle from './views/workspace/single-workspace.vue'

Vue.use(Router);

Vue.mixin({
    methods: {
        handleError(obj){
            if(obj.response){
                console.log(obj.response);
                if(obj.response.data.status === 401 || obj.response.data.status === 429){
                    axios.get('/auth/logout');
                    store.commit('logout');
                    this.$store.commit('setProjectKw', false);
                    this.$router.push('/login');
                }
            }
        },
    }
});

function authCheck (to, from, next) {
    if (store.state.auth) {
        axios.post('user/token-check', {token: store.state.apikey})
            .then(response => {
                // re-add header in case of refresh
                axios.defaults.headers.common['authentication'] = store.state.apikey;
                axios.defaults.headers.common['Content-Type'] = 'application/json';
                if(to.name === 'login'){
                    next({
                        path: '/'
                    })
                }else{
                    // Close data sidebar menus and misc sidebars
                    if (store.state.dataMenuOpen) {
                        store.commit('toggleDataMenu', false);
                    }
                    store.commit('setProjectKw', false);
                    next()
                }
            })
            .catch(e => {
                store.commit('logout');
                next({
                    path: '/login'
                })
            })

    }else{
        if(to.name === 'login'){
            next()
        } else {
            next({
                path: '/login'
            })
        }
    }
}

export default new Router({
    mode: 'history',
    // base: process.env.BASE_URL,
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
            beforeEnter: authCheck
        },
        {
            path: '/',
            name: 'home',
            component: () => import('./views/workspace/view-all.vue'),
            beforeEnter: authCheck
        },
        {
            path: '/workspaces',
            name: 'workspaces',
            component: () => import('./views/workspace/view-all.vue'),
            beforeEnter: authCheck
        },
        {
            path: '/workspaces/:workspaceslug',
            name: 'workspace single',
            component: () => import('./views/workspace/single-workspace.vue'),
            beforeEnter: authCheck
        },
        {
            path: '/workspaces/:workspaceslug/projects/:projectslug',
            name: 'project single',
            component: () => import('./views/project/single-project.vue'),
            beforeEnter: authCheck
        },
        {
            path: '/workspaces/:workspaceslug/projects/:projectslug/kwconfig',
            name: 'project analysis',
            component: () => import('./views/project/kwconfig.vue'),
            beforeEnter: authCheck
        },
        {
            path: '/tasks',
            name: 'tasks',
            component: () => import(/* webpackChunkName: "about" */ './views/Tasks.vue'),
            beforeEnter: authCheck
        },
        {
            path: '/*',
            name: '404',
            component: NotFound,
            beforeEnter: authCheck
        }
    ]
})
