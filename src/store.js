import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        dataMenuOpen: false,
        auth: false,
        apikey: '',
        alerts: [],
        projectKw: false
    },
    mutations: {
        toggleDataMenu(state, status) {
            if(status !== undefined) {
                state.dataMenuOpen = status;
            } else {
                state.dataMenuOpen = !state.dataMenuOpen
            }
        },
        login (state, apikey){
            state.apikey = apikey
            state.auth = true
            axios.defaults.headers.common = {
                "authentication": apikey,
            };
        },
        logout (state){
            state.apikey = ''
            state.auth = false
            state.userData = {}
            state.alerts = []
            axios.defaults.headers.common = {
                "authentication": ''
            };
        },
        storeAlert (state, alert){
            state.alerts.push(alert);
        },
        clearAlerts (state){
            state.alerts = []
        },
        setProjectKw(state, kwString){
            state.projectKw = kwString;
        }
    },
    actions: {

    }
})
