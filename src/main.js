import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import toasted from 'vue-toasted'
import store from './store'
import VueApexCharts from 'vue-apexcharts'
import customCore from './assets/custom'

export const bus = new Vue()

Vue.config.productionTip = false

Vue.use(toasted, {
    iconPack : 'fontawesome'
});

axios.defaults.headers.common = {
    "authentication": store.state.apikey,
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
};

Vue.use(VueApexCharts);

axios.defaults.baseURL = 'http://api.lacuna.com/api/v1/'
// axios.defaults.baseURL = 'http://localhost:8000/api/v1/'

let bodyClass = '';

// let imgPath = 'http://'

Vue.prototype.axios = axios;

new Vue({
    router,
    store,
    customCore,
  render: h => h(App)
}).$mount('#app')
